var zookeeper = require('node-zookeeper-client');
//集群以逗号隔开
var CONNECTION_STRING = 'localhost:2181';
var OPTIONS = {
    sessionTimeout: 5000
};

var zk = zookeeper.createClient(CONNECTION_STRING, OPTIONS);
zk.on('connected', function () {
    // 获取会话
    console.log("start");
    // 创建节点
    zk.create('/foo', new Buffer('hello'), function (error, path) {
        console.log('created:' + path + ",value:" + "hello");
    });
    // 节点存在
    zk.exists('/foo', function (error, stat) {
        if (stat)
        console.log('exists');
    });
    // 更新节点数据
    zk.setData('/foo', new Buffer('hi'), function (error, stat) {
        console.log('updated:' + stat + ",value:" + "hi");
    });
    // 获取节点数据
    zk.getData('/foo', function (error, data, stat) {
        console.log("get foo：" + data.toString());
    });
    // 列出子节点
    zk.getChildren('/', function (error, children, stat) {
        console.log(children);
    });
    // 删除节点
    zk.remove('/foo', function (error) {
        console.log(true);
    });
    zk.close();
});
zk.connect();